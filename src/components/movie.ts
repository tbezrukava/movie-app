import { IMovie } from '../helpers/typeHelpers/interfaces';
import { createMovieTemplate } from '../helpers/domHelpers/createFunctions';
import { handleClickMain, handleClickFavorite } from '../helpers/handlers/favBtnHandlers';
import { movieCardClasses } from '../constants/enums';

export default class Movie {
    protected options: IMovie

    constructor({ id, overview, poster, release }:IMovie) {
        this.options = { id, overview, poster, release }
    }

    createMainMovie(): HTMLElement {
        return createMovieTemplate({
            ...this.options,
            props: {
                className: movieCardClasses.main,
                attributes: {
                    id: `${this.options.id}`
                }
            },
            clickHandler: handleClickMain
        })
    }

    createFavoriteMovie(): HTMLElement {
        return createMovieTemplate({
            ...this.options,
            props: {
                className: movieCardClasses.favorite
            },
            clickHandler: handleClickFavorite
        })
    }
}