import initStorage from './storage/initStorage';
import renderMovies from './renders/renderMovies';
import renderRandomMovie from './renders/renderRandomMovie';
import renderFavoriteMovies from './renders/renderFavoriteMovies';

export async function render(): Promise<void> {
    const navBar = document.querySelector('.navbar-toggler') as Element;
    navBar.addEventListener('click', renderFavoriteMovies);

    initStorage();
    renderRandomMovie();
    renderMovies();
}
