const ENV = {
    API_URL: process.env.API_URL,
    IMG_URL: process.env.IMG_URL,
    API_KEY: process.env.API_KEY
};

export { ENV };