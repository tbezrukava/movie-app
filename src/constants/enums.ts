enum favBtnColors {
    transparent = 'rgba(255, 0, 0, 0.47)',
    red = 'red'
}

enum movieCardClasses {
    main = 'col-lg-3 col-md-4 col-12 p-2',
    favorite = 'col-12 p-2'
}

export { favBtnColors, movieCardClasses };