import { ENV } from './env';

const endpoints = {
    popular: (page:number):string => `${ENV.API_URL}movie/popular?api_key=${ENV.API_KEY}&page=${page}`,
    topRated: (page:number):string =>`${ENV.API_URL}movie/top_rated?api_key=${ENV.API_KEY}&page=${page}`,
    upcoming: (page:number):string => `${ENV.API_URL}movie/upcoming?api_key=${ENV.API_KEY}&page=${page}`,
    byId: (id:number):string => `${ENV.API_URL}movie/${id}?api_key=${ENV.API_KEY}`,
    search: (page:number, query:string):string => `${ENV.API_URL}search/movie?api_key=${ENV.API_KEY}&page=${page}&query=${query}`,
};

const imagePath = {
    poster: `${ENV.IMG_URL}w500`,
    backdrop: `${ENV.IMG_URL}original`,
    defaultPoster: 'src/assets/images/default-poster.jpg'
};

const moviesPerPage = 20;


export { endpoints, imagePath, moviesPerPage };