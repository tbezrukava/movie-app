import ApiService from '../services/api.service';
import Movie from '../components/movie';

export default function renderFavoriteMovies(): void {
    const service = new ApiService();
    const favoriteMoviesContainer = document.getElementById('favorite-movies') as HTMLElement;
    favoriteMoviesContainer.innerHTML = '';

    const favoriteMovies = JSON.parse(localStorage.favoriteMovies);

    favoriteMovies.map((id: number) => {
        service.getById(id)
            .then(res => {
                const movie = new Movie(res);
                favoriteMoviesContainer.appendChild(movie.createFavoriteMovie());
            })
            .catch(err => console.log(err));
    })
}