import { createLoadMoreBtn } from '../helpers/domHelpers/createFunctions';
import Movie from '../components/movie';
import { FetchFunc } from '../helpers/typeHelpers/interfaces';

export default function renderMovieCategory(fetchFunction:FetchFunc, query?: string):void {
    let currentPage = 1

    const container = document.getElementById('film-container') as HTMLElement;
    const loadMoreContainer = document.querySelector('.load-more-container') as HTMLElement;
    container.innerHTML = '';
    loadMoreContainer.innerHTML = '';

    const loadMoreBtn = createLoadMoreBtn();
    loadMoreBtn.addEventListener('click', load);
    loadMoreContainer.appendChild(loadMoreBtn);

    load();

    function load() {
        fetchFunction(currentPage, query)
            .then(res => {
                const {totalPages, movies} = res;
                if (totalPages) {
                    const moviesArray = movies.map(data => {
                        const movie = new Movie(data);
                        return movie.createMainMovie();
                    });

                    container.append(...moviesArray)

                    if(currentPage < totalPages) {
                        loadMoreBtn.style.display = 'block';
                        currentPage += 1;
                    } else {
                        loadMoreBtn.style.display = 'none';
                    }
                } else {
                    const message = document.createElement('p');
                    message.innerHTML = 'Nothing was found. Try another movie';
                    container.appendChild(message)
                }
            })
            .catch(err => console.log(err));
    }
}