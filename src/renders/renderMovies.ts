import ApiService from '../services/api.service';
import renderMovieCategory from './renderMovieCategory';

export default function renderMovies(): void {
    const service = new ApiService();
    const { getPopular, getUpcoming, getTopRated, getBySearch } = service;
    const popularBtn = document.getElementById('popular') as HTMLElement;
    const upcomingBtn = document.getElementById('upcoming') as HTMLElement;
    const topRatedBtn = document.getElementById('top_rated') as HTMLElement;
    const searchForm = document.querySelector('form') as HTMLElement;
    const search = document.getElementById('search') as HTMLInputElement;

    renderMovieCategory(getPopular);

    popularBtn.addEventListener('click', () => renderMovieCategory(getPopular));
    upcomingBtn.addEventListener('click', () => renderMovieCategory(getUpcoming));
    topRatedBtn.addEventListener('click', () => renderMovieCategory(getTopRated));

    searchForm.addEventListener('submit', (e) => {
        e.preventDefault();
        const searchQuery = search.value.trim()
        if(searchQuery) {
            renderMovieCategory(getBySearch, searchQuery)
            search.value = ''
        }
    })
}