import ApiService from '../services/api.service';

export default function renderRandomMovie(): void {
    const service = new ApiService();
    const randomMovieContainer = document.getElementById('random-movie') as HTMLElement;
    const movieName = document.getElementById('random-movie-name') as HTMLElement;
    const movieDescription = document.getElementById('random-movie-description') as HTMLElement;

    service.getRandomMovie()
        .then((movie) => {
            const { title, overview, backdrop } = movie;
            randomMovieContainer.style.backgroundImage = `url(${backdrop})`;
            movieName.innerHTML = title;
            movieDescription.innerHTML = overview;
        })
        .catch(err => console.log(err));
}