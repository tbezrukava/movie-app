import { endpoints, imagePath, moviesPerPage } from '../constants/api';
import { getRandomNumber } from '../helpers/common/common';
import { IResponseData, IGetMoviesResponse, IMovie, IRandomMovie, FetchFunc } from '../helpers/typeHelpers/interfaces';

export default class ApiService {
    getPopular: FetchFunc = (page) => {
        const url = endpoints.popular(page);
        return this.getMovies(url);
    }

    getTopRated: FetchFunc = (page) => {
        const url = endpoints.topRated(page);
        return this.getMovies(url);
    }

    getUpcoming: FetchFunc = (page) => {
        const url = endpoints.upcoming(page);
        return this.getMovies(url);
    }

    getBySearch: FetchFunc = (page, query) => {
        const url = endpoints.search(page, (query as string));
        return this.getMovies(url);
    }


    getById = async(id:number): Promise<IMovie> => {
        const url = endpoints.byId(id);
        const res = await this.fetchData(url);
        return this.transformMovie(res)
    }

    getRandomMovie = async(): Promise<IRandomMovie> => {
        const url = endpoints.popular(1);
        const res = await this.fetchData(url);
        const randomNum = getRandomNumber(0, moviesPerPage - 1);
        const randomMovie = (res.results as [])[randomNum];
        return this.transformRandomMovie(randomMovie);
    }

    protected fetchData = async (url:string): Promise<IResponseData | never> => {
        const res = await fetch(url);
        if(!res.ok) {
            throw new Error(`Could not fetch ${url}, received ${res.status}`);
        }

        return res.json();
    }

    protected getMovies = async(url: string): Promise<IGetMoviesResponse> => {
        const res = await this.fetchData(url);
        return (
            {
                totalPages: (res.total_pages as number),
                movies: (res.results as []).map(this.transformMovie)
            }
        );
    }

    protected transformMovie = (movie: IResponseData):IMovie => {
        const { id: movieId, overview: movieOverview, poster_path, release_date } = movie;
        return {
            id: movieId as number,
            overview: movieOverview as string || 'Plot unknown.',
            poster: poster_path ? `${imagePath.poster}${poster_path}` : imagePath.defaultPoster,
            release: release_date ? release_date as string : 'unknown'
        }
    }

    protected transformRandomMovie = (movie: IResponseData): IRandomMovie => {
        const { title: movieTitle, overview: movieOverview, poster_path, backdrop_path} = movie;
        return {
            title: movieTitle as string,
            overview: movieOverview as string || 'Plot unknown.',
            backdrop: backdrop_path ? `${imagePath.backdrop}${backdrop_path}` : `${imagePath.poster}${poster_path}`
        }
    }
}