interface IMovie {
    id: number;
    overview: string;
    poster: string;
    release: string;
}

interface  IRandomMovie {
    title: string,
    overview: string,
    backdrop: string
}

interface IResponseData {
    [key: string]: unknown
}

interface IGetMoviesResponse {
    totalPages: number,
    movies: IMovie[]
}

interface IElementParams {
    tagName: string;
    className?: string;
    attributes?: {[key: string]: string}
}

interface ICreateTemplate extends IMovie{
    props: {
        className: string,
        attributes?: {[key: string]: string}
    }
    clickHandler: ClickHandlerFunc
}

interface CreateElementFunc {
    ({tagName, className, attributes}:IElementParams): HTMLElement
}

interface CreateElementNSFunc {
    ({tagName, className, attributes}:IElementParams): SVGElement
}

interface ClickHandlerFunc {
    (id: number): void
}

interface CreateTemplateFunc {
    ({id, overview, poster, release, props, clickHandler }:ICreateTemplate): HTMLElement;
}

interface FetchFunc {
    (page: number, query?: string): Promise<IGetMoviesResponse>;
}

export {
    IMovie,
    IRandomMovie,
    IResponseData,
    IGetMoviesResponse,
    CreateElementFunc,
    CreateElementNSFunc,
    CreateTemplateFunc,
    ClickHandlerFunc,
    FetchFunc
};