import { CreateElementFunc, CreateElementNSFunc, CreateTemplateFunc } from '../typeHelpers/interfaces';
import { favBtnColors } from '../../constants/enums';

const createElement:CreateElementFunc = function({ tagName, className, attributes = {} }) {
    const element = document.createElement(tagName);

    if (className) {
        const classNames = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

    return element;
}

const createElementNS:CreateElementNSFunc = function({ tagName, className, attributes = {} }) {
    const xmlns = "http://www.w3.org/2000/svg";
    const element = document.createElementNS(xmlns, tagName);

    if (className) {
        const classNames = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    Object.keys(attributes).forEach((key) => element.setAttributeNS(null, key, attributes[key]));

    return element;
}

const createLoadMoreBtn = ():HTMLElement => {
    const btn = createElement({
        tagName: 'button',
        className: 'btn btn-lg btn-outline-success',
        attributes: {
            id: 'load-more',
            type: 'button'
        }
    });

    btn.innerHTML = 'Load more';

    return btn;
};

const createMovieTemplate:CreateTemplateFunc = function (data ) {
    const { id, poster, overview, release, props, clickHandler } = data;
    const favoriteMovies = JSON.parse(localStorage.favoriteMovies);
    const isFavorite = favoriteMovies.includes(id);
    const btnColor = isFavorite? favBtnColors.red : favBtnColors.transparent;

    const movie = createElement({
        tagName: 'div',
        ...props
    });

    const card = createElement({
        tagName: 'div',
        className: 'card shadow-sm'
    });

    const image = createElement({
        tagName: 'img',
        attributes: {
            src: poster,
            alt: 'poster'
        }
    });

    const cardBody = createElement({
        tagName: 'div',
        className: 'card-body'
    });

    const cardText = createElement({
        tagName: 'p',
        className: 'card-text truncate'
    });

    const releaseDate = createElement({
        tagName: 'div',
        className: 'd-flex justify-content-between align-items-center'
    });

    const releaseContent = createElement({
        tagName: 'small',
        className: 'text-muted'
    });

    const favBtn = createElementNS({
        tagName: 'svg',
        className: 'bi bi-heart-fill position-absolute p-2',
        attributes: {
            stroke: 'red',
            fill: btnColor,
            width: '50',
            height: '50',
            viewBox: '0 -2 18 22'
        }
    });

    const svgPath = createElementNS({
        tagName: 'path',
        attributes: {
            'fill-rule': 'evenodd',
            d: 'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z'
        }
    });


    cardText.innerHTML = overview;
    releaseContent.innerHTML = release;

    favBtn.appendChild(svgPath);
    releaseDate.append(releaseContent);
    cardBody.append(cardText, releaseDate);
    card.append(image, favBtn, cardBody);
    movie.append(card);

    favBtn.addEventListener('click', () => clickHandler(id));

    return movie;
}



export { createMovieTemplate, createLoadMoreBtn };