import { favBtnColors } from '../../constants/enums';
import renderFavoriteMovies from '../../renders/renderFavoriteMovies';
import { ClickHandlerFunc } from '../typeHelpers/interfaces';

function getBtn(id: number): SVGElement {
    const container = document.getElementById(id.toString()) as HTMLElement;
    return container.querySelector('svg') as SVGElement;
}

const handleClickFavorite: ClickHandlerFunc =  function (id) {
    const favoriteMovies = JSON.parse(localStorage.getItem('favoriteMovies') as string);
    const movieIdx = favoriteMovies.indexOf(id);
    localStorage.favoriteMovies = JSON.stringify([
        ...favoriteMovies.slice(0, movieIdx),
        ...favoriteMovies.slice(movieIdx + 1)
    ]);
    const btn = getBtn(id);
    btn.setAttributeNS(null, 'fill', favBtnColors.transparent);
    renderFavoriteMovies();
}

const handleClickMain: ClickHandlerFunc = function (id) {
    const favoriteMovies = JSON.parse(localStorage.getItem('favoriteMovies') as string);
    const isFavorite = favoriteMovies.includes(id);
    const btn = getBtn(id);
    if (isFavorite) {
        btn.setAttributeNS(null, 'fill', favBtnColors.transparent);
        const movieIdx = favoriteMovies.indexOf(id);
        localStorage.favoriteMovies = JSON.stringify([
            ...favoriteMovies.slice(0, movieIdx),
            ...favoriteMovies.slice(movieIdx + 1)
        ]);
    } else {
        btn.setAttributeNS(null, 'fill', favBtnColors.red);
        localStorage.setItem('favoriteMovies', JSON.stringify([...favoriteMovies, id]));
    }
}

export { handleClickFavorite, handleClickMain };