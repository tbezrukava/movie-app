export default function initStorage(): void {
    const key = 'favoriteMovies';
    if (!Object.keys(localStorage).includes(key)) {
        localStorage.setItem(key, JSON.stringify([]));
    }
};